#!/bin/bash
cd "$(dirname "$0")"
echo "Installing main wimpysworld UMPC files..."
./umpc-ubuntu.sh enable
echo "Fixing EDID firmware permissions..."
chmod 755 /lib/firmware/edid
chmod 644 /lib/firmware/edid/*
echo "Adding EDID firmware to initramfs for early DRM..."
cp data/initramfs-edid /etc/initramfs-tools/hooks/edid
update-initramfs -u
echo "Adding service to sync main user's monitor settings to GDM for rotated login..."
cp data/sync-gdm-monitors.service /etc/systemd/system
cp data/sync-monitors.sh /usr/sbin/
chmod +x /usr/sbin/sync-monitors.sh
systemctl daemon-reload
systemctl enable sync-gdm-monitors
